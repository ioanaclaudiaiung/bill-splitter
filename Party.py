import random

num_of_friends = int(input("Enter the number of friends joining (including you):\n"))
print()
dict_friends = {}

if num_of_friends <= 0:
    print("No one is joining for the party")
else:
    print("Enter the name of every friend (including you), each on a new line:")
    for i in range(num_of_friends):
        new_friend = input()
        dict_friends.update({new_friend: 0})  # dict_friends[new_friend] = 0

    bill = float(input("\nEnter the total bill value:\n"))
    print()
    lucky = input('Do you want to use the "Who is lucky?" feature? Write Yes/No:\n')
    print()

    if lucky == "Yes":
        for i in dict_friends.keys():
            dict_friends[i] = round((bill / (num_of_friends - 1)), 2)
        list_friends = list(dict_friends.keys())
        choise = random.choice(list_friends)

        dict_friends.update({choise: 0})
        print(f"{choise} is the lucky one!")

        print("\n{}".format(dict_friends))

    else:
        print("No one is going to be lucky\n")
        for new_friend in dict_friends.keys():
            dict_friends[new_friend] = int(bill / num_of_friends)
        else:
            for new_friend in dict_friends.keys():
                dict_friends[new_friend] = round((bill / num_of_friends), 2)
        print("\n{}".format(dict_friends))
